package com.boa.elevator;

import java.util.List;

/**
 * Main service interface with default methods.
 * 
 * @author Aadarsh
 *
 */
public interface ElevatorMode {
	/**
	 * core execution method for the elevator operation.
	 * 
	 * @param path
	 *            list of floors to be visited.
	 * @param st_flr
	 *            starting floor
	 * @param cmd_list
	 *            list of elevator pick and drop floor commands.
	 * @return distance total number of floors traveled.
	 */
	int executeCommands(List<Integer> path, int st_flr, String[] cmd_list);

	/**
	 * Utility method to add floors to be visited based on the starting and
	 * ending floors.
	 * 
	 * @param stops
	 *            list of integers
	 * @param st_flr
	 *            starting floors
	 * @param end_flr
	 *            ending floors
	 */
	default void addFloors(List<Integer> stops, int st_flr, int end_flr) {
		stops.add(st_flr);
		stops.add(end_flr);
	}

	/**
	 * Moves the elevator to differnt floors based on the starting and ending
	 * floors and calculates the path.
	 * 
	 * @param path
	 *            collection of integers.
	 * @param st_flr
	 *            starting floor.
	 * @param end_flr
	 *            ending floor
	 * @return returns difference between the starting and ending floors.
	 */
	default int move(List<Integer> path, int st_flr, int end_flr) {
		if (end_flr == st_flr)
			return 0;
		path.add(end_flr);
		return Math.abs(end_flr - st_flr);
	}

	/**
	 * Calculates the distance between the stop and end floors.
	 * 
	 * @param path
	 *            collection of integers.
	 * @param st_flr
	 *            starting floor.
	 * @param stops
	 *            floors to be visited.
	 * 
	 * @return dist distance traveled.
	 */
	default int travel(List<Integer> path, int st_flr, List<Integer> stops) {
		int tmp_st_flr = st_flr;
		int dist = 0;
		for (Integer stop : stops) {
			dist += move(path, tmp_st_flr, stop);
			tmp_st_flr = stop;
		}
		return dist;
	}
}
