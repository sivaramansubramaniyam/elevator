package com.boa.elevator.enums;

/**
 * Enum for holding elevator moving direction.
 * 
 * @author Aadarsh
 *
 */
public enum Direction {
	UP, DOWN
}
