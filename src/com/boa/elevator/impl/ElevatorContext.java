package com.boa.elevator.impl;

import java.util.ArrayList;
import java.util.List;

import com.boa.elevator.ElevatorMode;

/**
 * Context class for elevator. Defines and holds the key attributes of the
 * computation.
 * 
 * @author Aadarsh
 *
 */
class ElevatorContext {
	/**
	 * Collection of integers for the travel path.
	 */
	private final List<Integer> path;
	/**
	 * Total distance traveled.
	 */
	private int distance;
	/**
	 * Elevator mode instance.
	 */
	private final ElevatorMode elevator_mode;

	/**
	 * Set the elevator mode instance to the this context.
	 * 
	 * @param mode
	 */
	public ElevatorContext(ElevatorMode mode) {
		this.elevator_mode = mode;
		this.path = new ArrayList<Integer>();
	}

	/**
	 * Core functional method for the elevator execution.
	 * 
	 * @param st_flr
	 *            starting floor
	 * @param cmd_list
	 *            list of elevator commands
	 */
	public void executeCommands(int st_flr, String[] cmd_list) {
		this.distance = this.elevator_mode.executeCommands(this.path, st_flr,
				cmd_list);
	}

	/**
	 * Utility method to clear the path collection.
	 */
	public void flushPath() {
		this.path.clear();
	}

	/**
	 * Getter method for path.
	 * 
	 * @return path List of integers.
	 */
	public List<Integer> getPath() {
		return path;
	}

	/**
	 * Getter method for distance traveled in floors.
	 * 
	 * @return distance floors visited.
	 */
	public int getDistance() {
		return distance;
	}
}