package com.boa.elevator.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

/**
 * <pre>
 * 
 * TEST INPUT: 
 * 
 * 10:8-1
 * 9:1-5,1-6,1-5
 * 2:4-1,4-2,6-8
 * 3:7-9,3-7,5-8,7-11,11-1
 * 7:11-6,10-5,6-8,7-4,12-7,8-9
 * 6:1-8,6-8
 * 
 * TEST OUTPUT: 
 * 
 * *********** MODE A *************
 * 10 8 1 (9)
 * 9 1 5 1 6 1 5 (30)
 * 2 4 1 4 2 6 8 (16)
 * 3 7 9 3 7 5 8 7 11 1 (36)
 * 7 11 6 10 5 6 8 7 4 12 7 8 9 (40)
 * 6 1 8 6 8 (16)
 * 
 * *********** MODE B **************
 * 10 8 1 (9)
 * 9 1 5 6 (13)
 * 2 4 2 1 6 8 (12)
 * 3 5 7 8 9 11 1 (18)
 * 7 11 10 6 5 6 8 12 7 4 8 9 (30)
 * 6 1 6 8 (12)
 * 
 * </pre>
 */

/**
 * Main execution class. Defines few private utility methods along with the main
 * method for the execution of the elevator.
 * 
 * @author Aadarsh
 *
 */
public class Elevator {

	/**
	 * main method for the JRE to execute.
	 * 
	 * @param args command line arguments
	 */
	public static void main(String[] args) {

		// validates the command line arguments
		validateArguments(args);

		String file_name = args[0];
		String mode = args[1];

		// validation method for the elevator mode arguments.
		validateElevatorModes(mode);

		try (BufferedReader bf = new BufferedReader(new FileReader(
				validateFile(file_name)))) {
			ElevatorContext elev = getElevatorContext(mode);

			String line;
			int st_floor, col_idx;
			String[] floor_cmds;
			StringBuilder outputStr = new StringBuilder(256);
			List<Integer> path;
			int tot_dist;
			while ((line = bf.readLine()) != null) {

				// Parse Input
				col_idx = line.indexOf(":");
				if (col_idx <= 0)
					continue;
				st_floor = Integer.parseInt(line.substring(0, col_idx));

				floor_cmds = line.substring(col_idx + 1).split(",");

				// Execute commands
				elev.executeCommands(st_floor, floor_cmds);
				path = elev.getPath();
				tot_dist = elev.getDistance();

				// Print output
				prepareOutput(outputStr, path, tot_dist);

				// reset
				elev.flushPath();
				outputStr.setLength(0);
			} // end of file
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * Validation method to make sure 2 command line arguments are entered for
	 * proper program execution.
	 * 
	 * @param args
	 *            command line agruments pass to main method.
	 */
	private static void validateArguments(String[] args) {
		if (args.length < 2) {
			System.out.println("usage: java Elevator <file_name> <mode[A|B]>");
			System.exit(1);
		}
	}

	/**
	 * Utility method to print the output on the system out.
	 * 
	 * @param outputStr
	 *            output string builder
	 * @param path
	 *            list of elevator path
	 * @param tot_dist
	 *            total distance traveled.
	 */
	private static void prepareOutput(StringBuilder outputStr,
			List<Integer> path, int tot_dist) {
		for (Integer flr : path) {
			outputStr.append(flr).append(" ");
		}
		outputStr.append("(").append(tot_dist).append(")");
		System.out.println(outputStr.toString());
	}

	/**
	 * Factory method to create elevator context object based on the given mode.
	 * 
	 * @param mode
	 *            elevator mode in string form.
	 * 
	 * @return ElevatorContext elevator context object.
	 */
	private static ElevatorContext getElevatorContext(String mode) {
		ElevatorContext elev;
		if (mode.equalsIgnoreCase("A")) {
			elev = new ElevatorContext(ModeAlpha.getInstance());
			System.out.println("------------------ Mode A ------------------");
		} else { // default
			elev = new ElevatorContext(ModeBeta.getInstance());
			System.out.println("------------------ Mode B ------------------");
		}
		return elev;
	}

	/**
	 * Validation method for the existence of the given file name. If no file
	 * exists on the given name then exit JVM execution.
	 * 
	 * @param file_name
	 *            file name as string.
	 * 
	 * @return file file object.
	 */
	private static File validateFile(String file_name) {
		File file = new File(file_name);
		if (!file.exists()) {
			System.out.println("Error! File Doesn't exist. File_name: "
					+ file_name);
			System.exit(1);
		}
		return file;
	}

	/**
	 * Validates the mode argument (2nd argument). Stops the program if the
	 * given argument is not in the acceptable value.
	 * 
	 * @param mode
	 *            elevator mode as string.
	 */
	private static void validateElevatorModes(String mode) {
		if (null == mode
				|| !(mode.equalsIgnoreCase("A") || mode.equalsIgnoreCase("B"))) {
			System.out.println("Error! Invalid MODE(" + mode
					+ "). Valid modes: 'A', 'B'");
			System.exit(1);
		}
	}
}
