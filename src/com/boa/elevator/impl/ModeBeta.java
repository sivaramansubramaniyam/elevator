package com.boa.elevator.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.boa.elevator.ElevatorMode;
import com.boa.elevator.enums.Direction;

/**
 * This class implements ElevatorMode interface and acknowledge commands in
 * sequence, transporting more people at a time.
 * 
 * @author Aadarsh
 *
 */
public class ModeBeta implements ElevatorMode {
	private static ModeBeta instance = null;

	/**
	 * Private constructor.
	 */
	private ModeBeta() {
	}

	/**
	 * Factory method to create an instance of the ModeBeta object.
	 * 
	 * @return ElevatorMode Instance of elevator mode.
	 */
	public static ElevatorMode getInstance() {
		if (instance == null) {
			instance = new ModeBeta();
		}
		return instance;
	}

	/**
	 * Based on the given commands identifies the stops, order of stops and
	 * calculate total distance traveled to complete the given command.
	 * 
	 * @return dist total distance as integer
	 */
	@Override
	public int executeCommands(List<Integer> path, int st_flr, String[] cmd_list) {
		int tmp_st_flr, tmp_end_flr;
		int dist = 0;
		Direction cur_dir = null, dir;
		String[] split;

		List<Integer> floorList = new ArrayList<Integer>();
		// add the starting floor
		path.add(st_flr);

		for (String cmd : cmd_list) {

			split = cmd.split("-");
			tmp_st_flr = Integer.parseInt(split[0]);
			tmp_end_flr = Integer.parseInt(split[1]);
			dir = getDirection(tmp_st_flr, tmp_end_flr);

			if (null == cur_dir || cur_dir == dir) {
				// add the floor
				addFloors(floorList, tmp_st_flr, tmp_end_flr);
				cur_dir = dir;
				continue;
			}

			sortFloors(cur_dir, floorList);
			// getInstance();
			// move
			dist += travel(path, st_flr, floorList);
			if (!floorList.isEmpty()) {
				st_flr = floorList.get(floorList.size() - 1);
			}

			floorList.clear();

			// add the floor
			addFloors(floorList, tmp_st_flr, tmp_end_flr);
			cur_dir = dir;
		}

		sortFloors(cur_dir, floorList);
		// move
		dist += travel(path, st_flr, floorList);

		return dist;
	}

	/**
	 * Utility method to sort the given list based on the direction. If the
	 * direction is DOWN then sort it in the descending order, if not ascending
	 * order.
	 * 
	 * @param cur_dir
	 *            Current Direction
	 * @param floorList
	 *            Floors List
	 */
	private void sortFloors(Direction cur_dir, List<Integer> floorList) {
		if (cur_dir == Direction.DOWN) {
			// desc order
			floorList.sort(Collections.reverseOrder());
		} else {
			// asc order
			floorList.sort(null);
		}
	}

	/**
	 * Returns the direction of the movement based on the starting and ending
	 * floor.
	 * 
	 * @param st_flr
	 *            Starting floor
	 * @param end_flr
	 *            Ending floor
	 * @return Direction up or down
	 */
	private Direction getDirection(int st_flr, int end_flr) {
		return (end_flr - st_flr) > 0 ? Direction.UP : Direction.DOWN;
	}
}
