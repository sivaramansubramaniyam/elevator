package com.boa.elevator.impl;

import java.util.ArrayList;
import java.util.List;

import com.boa.elevator.ElevatorMode;

/**
 * This class implements ElevatorMode interface and acknowledge commands in
 * sequence, transport one people at a time.
 * 
 * @author Aadarsh
 */
public class ModeAlpha implements ElevatorMode {
	private static ModeAlpha instance = null;

	/**
	 * Private constructor.
	 */
	private ModeAlpha() {
	}

	/**
	 * Factory method to create an instance of the ModeAlpha object.
	 * 
	 * @return ElevatorMode elevator mode instance.
	 */
	public static ElevatorMode getInstance() {
		if (null == instance) {
			instance = new ModeAlpha();
		}
		return instance;
	}

	/**
	 * Based on the given commands identifies the stops, order of stops and
	 * calculate total distance traveled to complete the given command.
	 * 
	 * @return dist total distance as integer
	 */
	@Override
	public int executeCommands(List<Integer> path, int st_flr, String[] cmd_list) {
		int tmp_st_flr, tmp_end_flr;
		int dist = 0;
		List<Integer> floorList = new ArrayList<Integer>();
		// add the starting floor
		path.add(st_flr);

		for (String cmd : cmd_list) {

			String[] split = cmd.split("-");
			tmp_st_flr = Integer.parseInt(split[0]);
			tmp_end_flr = Integer.parseInt(split[1]);

			// collect floors
			addFloors(floorList, tmp_st_flr, tmp_end_flr);

			// move the elevator to caluate the distance travelled.
			dist += travel(path, st_flr, floorList);
			if (!floorList.isEmpty()) {
				st_flr = floorList.get(floorList.size() - 1);
			}
			floorList.clear();
		}
		return dist;
	}
}
