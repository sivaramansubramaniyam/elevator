package com.boa.elevator.impl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

import com.boa.elevator.ElevatorMode;

/**
 * JUnit test class for ModeBeta class methods.
 * 
 * @author Aadarsh
 *
 */
public class ModeAlphaTest {

	// Test Case for path : 10:8-1
	@Test
	public final void testExecuteCommands0() {
		ElevatorMode modeA = ModeAlpha.getInstance();
		String[] cmdList = { "8-1" };
		int distance = modeA.executeCommands(new ArrayList<Integer>(), 10,
				cmdList);
		assertEquals(9, distance);
	}

	// Test Case for path : 9:1-5,1-6,1-5
	@Test
	public final void testExecuteCommands1() {
		ElevatorMode modeA = ModeAlpha.getInstance();
		String[] cmdList = { "1-5", "1-6", "1-5" };
		int distance = modeA.executeCommands(new ArrayList<Integer>(), 9,
				cmdList);
		assertEquals(30, distance);
	}

	// Test Case for path : 2:4-1,4-2,6-8
	@Test
	public final void testExecuteCommands2() {
		ElevatorMode modeA = ModeAlpha.getInstance();
		String[] cmdList = { "4-1", "4-2", "6-8" };
		int distance = modeA.executeCommands(new ArrayList<Integer>(), 2,
				cmdList);
		assertEquals(16, distance);
	}

	// Test Case for path : 3:7-9,3-7,5-8,7-11,11-1
	@Test
	public final void testExecuteCommands3() {
		ElevatorMode modeA = ModeAlpha.getInstance();
		String[] cmdList = { "7-9", "3-7", "5-8", "7-11", "11-1" };
		int distance = modeA.executeCommands(new ArrayList<Integer>(), 3,
				cmdList);
		assertEquals(36, distance);
	}

	// Test Case for path : 7:11-6,10-5,6-8,7-4,12-7,8-9
	@Test
	public final void testExecuteCommands4() {
		ElevatorMode modeA = ModeAlpha.getInstance();
		String[] cmdList = { "11-6", "10-5", "6-8", "7-4", "12-7", "8-9" };
		int distance = modeA.executeCommands(new ArrayList<Integer>(), 7,
				cmdList);
		assertEquals(40, distance);
	}

	// Test Case for path : 6:1-8,6-8
	@Test
	public final void testExecuteCommands5() {
		ElevatorMode modeA = ModeAlpha.getInstance();
		String[] cmdList = { "1-8", "6-8" };
		int distance = modeA.executeCommands(new ArrayList<Integer>(), 6,
				cmdList);
		assertEquals(16, distance);
	}
}
